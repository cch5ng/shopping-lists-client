# [WIP] shopping-lists-client

## Purpose

* shopping list manager
* a MVP is available at https://seedtosoul.surge.sh/

### Features

  * ability to save templates
  * ability to create list from template or blank slate
  * ability to mark list items as purchased
  * ability to use from mobile device

  * (TODO) multi-device support
  * (TODO) ability to order items by store section

### Limitations

  * currently there is a 15 item max per list (bug)
  * currently there is not multi-device support (this is a client-side app currently)
  * currently deleting a list is not supported
  * currently changes are only stored per browser session

## TODO

* this project will also be supported by (todo) shopping-lists-server


## Resources

This project was bootstrapped using mern-starter-client and create-react-app

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


## Caveats

There is some likelihood that this project may become out of date in the future (some of the dependent npm modules might get upgraded by their maintainers and might not get updated in this project). Sorry if this occurs.
