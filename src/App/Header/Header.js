import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './style.css';

class Header extends Component {

  render() {
    return (
      <div className="header title">
        <div className="header-main">
          <h2><Link to="/" className="header-main">Seed to Soul</Link></h2>
        </div>
        <div className="header-contact">
          <p className="nav-link">
            <Link to="/lists">Lists</Link>
          </p>
          <p className="nav-link">
            <Link to="/listsNew">New List</Link>
          </p>
          <p className="nav-link">
            <Link to="/settings/listTemplates">Templates</Link>
          </p>
          <p className="nav-link">
            <Link to="/settings/store_sections">Store Sections</Link>
          </p>
        </div>


      </div>
    )
  }
}

//{/*<Link to="/">Posts</Link> <Link to="/newPost">Add Post</Link>*/}


export default Header