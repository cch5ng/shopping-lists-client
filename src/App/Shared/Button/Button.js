import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './style.css';

class Button extends Component {

  render() {
    let { label, onClickHandler, classVal, idVal } = this.props

    return (
      <button className="button"  onClick={ev => onClickHandler(ev)} >{label}</button>
    )
  }
}

export default Button

//className={classVal}
