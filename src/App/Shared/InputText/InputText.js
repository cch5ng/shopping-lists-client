import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './style.css';

class InputText extends Component {

// TODO track the value via component state; create onChange handler to manage state
// but internal onChange should probably call a prop onChange that would pass value to some
// dispatch action call

  render() {
    let {defVal, options, placeholderVal, idVal, onChangeHandler} = this.props
    return (
      <input type="text" className="list-item-input" id={idVal} value={defVal} 
        onChange={ev => onChangeHandler(ev)}
        placeholder={placeholderVal} />
    )
  }
}

export default InputText