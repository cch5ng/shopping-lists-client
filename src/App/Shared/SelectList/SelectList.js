import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './style.css';

class SelectList extends Component {

// TODO track the value via component state; create onChange handler to manage state
// but internal onChange should probably call a prop onChange that would pass value to some
// dispatch action call

  render() {
    let {defVal, options, onChange, idVal} = this.props
    return (
      <select  id={idVal} value={defVal} onChange={(ev) => onChange(ev)}>
        {options.map((option, idx) => {
          let key = `${option['value']}${idx.toString()}`
          return (<option key={key} value={option['value']} >{option['label']}</option>)
        })}
      </select>
    )
  }
}

export default SelectList

//key={idVal}

