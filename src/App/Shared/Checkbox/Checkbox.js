import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './style.css';

class Checkbox extends Component {

  render() {
    let { checkboxVal, idVal, onChangeHandler, checkboxLabel, itemId } = this.props

    return (
      <div className="div-checkbox">
        <input checked={checkboxVal} type="checkbox" className="list-item-input" id={idVal} 
          onChange={ev => onChangeHandler(ev)} />
        <label className="checkbox-label" onClick={ev => onChangeHandler(ev)} id={itemId} />
      </div>
    )
  }
}

export default Checkbox
