import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './style.css';

class Footer extends Component {

  render() {
    return (
      <div className="footer title">
        <hr />
        <div className="">
          <p>Shopping Lists Manager</p>
          <p>✧ was made with ☕️ by <a href="https://github.com/cch5ng/shopping-lists-client" className="body-link" target="_blank">cch5ng</a> ✧</p>
          <p>Copyright 2017</p>
        </div>
      </div>
    )
  }
}

export default Footer
