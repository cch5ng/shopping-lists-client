// action to edit list order for default section list
export const REQUEST_EDIT_SECTION_LIST = 'REQUEST_EDIT_SECTION_LIST'
export const RECEIVE_EDIT_SECTION_LIST = 'RECEIVE_EDIT_SECTION_LIST'

export function requestSectionListEdit() {
  return {
    type: REQUEST_EDIT_SECTION_LIST,
    retrieving: true
  }
}

export function receiveSectionListEdit(reorderedSectionsObj) {
  //let normListObj = {}
  //let listId = listObj.listId
  //normListObj[listId] = listObj

  return {
    type: RECEIVE_EDIT_SECTION_LIST,
    reorderedSectionsObj,
    retrieving: false
  }
}