import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import FaEdit from 'react-icons/lib/fa/edit';
import uuidv1 from 'uuid/v1';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';

import '../App.css';

import { requestSectionListEdit, receiveSectionListEdit } from '../SectionLists/SectionListsActions';

const grid = 8;

class SectionListsContainer extends Component {

  constructor(props) {
    super(props);

    // this.state = {
    // }

    this.objToSortedArray = this.objToSortedArray.bind(this);
    this.getItemStyle = this.getItemStyle.bind(this);
    this.getListStyle = this.getListStyle.bind(this);
    this.reorder = this.reorder.bind(this);
    this.onDragEnd = this.onDragEnd.bind(this);
    //onChangeHandlerSelectSort = this.onChangeHandlerSelectSort.bind(this)
    //onClickHandlerDeleteList = this.onClickHandlerDeleteList.bind(this)

  }



  //handle sort order change
  // onChangeHandlerSelectSort(ev) {
  //   let templateId = ev.target.value
  //   let listId = uuidv1()
  //   let listName = `Copy of ${this.props.listTemplates[templateId].listName}`
  //   let listItemInputs = this.props.listTemplates[templateId].listItemInputs
  //   let requestBody = { listId, listName, listItemInputs}

  //   this.props.receiveListCreate(requestBody)
  // }

  objToSortedArray(sectionList) {
    let sortedAr = [];

    return sortedAr;
  }

  //TODO refactor to update redux store for section order
  onDragEnd(result) {
    // dropped outside the list
    if (!result.destination) {
      return;
    }

    let items = this.reorder(
      this.sectionListsAr,
      result.source.index,
      result.destination.index
    );

    // update the order properties
    let items2Obj = {};

    items.forEach((item, idx) => {
      //console.log('item', item);
      let newItem = {};
      let id = item.id;
      newItem.order = idx + 1;
      newItem.id = id;
      newItem.name = item.name;

      items2Obj[id] = newItem;
    });

    console.log('items2Obj', items2Obj);

    this.props.receiveSectionListEdit(items2Obj);
  }

  // a little function to help us with reordering the result
  // https://codesandbox.io/s/k260nyxq9v
  reorder(list, startIndex, endIndex) {
    let result = list.slice();
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  }

  getItemStyle(isDragging, draggableStyle) {
    // some basic styles to make the items look a bit nicer
    return { userSelect: 'none',
      padding: grid * 2,
      margin: `0 0 ${grid}px 0`,

      // change background colour if dragging
      background: isDragging ? '#e5e5e5' : 'white',
      border: isDragging ? '1px dashed #323232' : '1px solid #000000',

      // styles we need to apply on draggables
      ...draggableStyle, }
  }

  getListStyle(isDraggingOver) {
    return {
      background: isDraggingOver ? '#7fc2c5' : 'lightgrey',
      padding: grid,
      width: 250,
      margin: '12px 0 12px 12px',
    }
  }

  render() {
    let sectionListsAr = [];
    let sections = this.props.sectionLists['id00001'].sections;
    if (this.props.sectionLists) {
      Object.keys(sections).forEach((k, idx) => {
        let order = sections[k].order;
        sectionListsAr[order - 1] = sections[k];
      });

      this.sectionListsAr = sectionListsAr;
    }

    return (
      <div>
      <h2>Drag to Reorder Sections</h2>

      <DragDropContext onDragEnd={this.onDragEnd}>
        <Droppable droppableId="droppable">
          {(provided, snapshot) => (
            <div
              ref={provided.innerRef}
              style={this.getListStyle(snapshot.isDraggingOver)}
            >
              {sectionListsAr.map((item, index) => (
                <Draggable key={item.id} draggableId={item.id} index={index}>
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      {...provided.draggableProps}
                      {...provided.dragHandleProps}
                      style={this.getItemStyle(
                        snapshot.isDragging,
                        provided.draggableProps.style
                      )}
                    >
                      {item.name}
                    </div>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </DragDropContext>
      </div>

    )
  }
}

function mapStateToProps({ sectionLists }) {
  return {
    sectionLists: sectionLists.sectionLists,
  }
}

function mapDispatchToProps(dispatch, ownProps) {
  return bindActionCreators( 
    { requestSectionListEdit, receiveSectionListEdit
    }, dispatch)
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SectionListsContainer))
