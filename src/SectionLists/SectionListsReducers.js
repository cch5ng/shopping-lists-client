import { RECEIVE_EDIT_SECTION_LIST, REQUEST_EDIT_SECTION_LIST } from './SectionListsActions'

//import { RECEIVE_ADD_LIST } from '../ListsActions'

const initialState = {
  sectionLists: {
    'id00001': {
      "listId": "idsl00001",
      "listName": "Grocery",
      "sections": {
        "sl001": {
          "id": "sl001",
          "name": "breads",
          "order": 1,
        },
        "sl002": {
          "id": "sl002",
          "name": "produce",
          "order": 2,
        },
        "sl003": {
          "id": "sl003",
          "name": "deli",
          "order": 3,
        },
        "sl004": {
          "id": "sl004",
          "name": "cheese",
          "order": 4,
        },
        "sl005": {
          "id": "sl005",
          "name": "sandwich meats",
          "order": 5,
        },
        "sl006": {
          "id": "sl006",
          "name": "dairy",
          "order": 6,
        },
        "sl007": {
          "id": "sl007",
          "name": "drinks",
          "order": 7,
        },
        "sl008": {
          "id": "sl008",
          "name": "frozen",
          "order": 8,
        },
        "sl009": {
          "id": "sl009",
          "name": "snacks",
          "order": 9,
        },
        "sl010": {
          "id": "sl010",
          "name": "toiletries",
          "order": 10,
        },
        "sl011": {
          "id": "sl011",
          "name": "household",
          "order": 11,
        },
        "sl012": {
          "id": "sl012",
          "name": "alcohol",
          "order": 12,
        },
      }
    }
  }
};

export function sectionLists (state = initialState, action) {
  let listObj = {}
  switch(action.type) {
    case RECEIVE_EDIT_SECTION_LIST:
      let defaultUpdated = {
        'id00001': {
          "listId": "idsl00001",
          "listName": "Grocery",
          "sections": {
            ...state['sectionLists']['id00001']['sections'],
            ...action.reorderedSectionsObj,
          }
        }
      }


      return {
        ...state,
        sectionLists: //defaultUpdated,
          { ...state.sectionLists, 
            'id00001': {
              ...state.sectionLists['id00001'],
              ['sections']: {
                ...state['sectionLists']['id00001']['sections'],
                ...action.reorderedSectionsObj,
              },
            }
            //...action.reorderedSectionsObj
          },
        retrieving: false
      }
    case REQUEST_EDIT_SECTION_LIST:
      return {
        ...state,
        retrieving: true
      }
    default:
      return state
  }
}
