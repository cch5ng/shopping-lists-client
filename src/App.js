import React, { Component } from 'react';
import {
  BrowserRouter as Router, Route } from 'react-router-dom'

import './App.css';
import Header from './App/Header/Header'
import Footer from './App/Footer/Footer'
import ListsContainer from './Lists/ListsContainer'
import ListDetailContainer from './Lists/ListDetailContainer'
import ListDetailFormContainer from './Lists/ListDetailFormContainer'
import ListTemplatesContainer from './ListTemplates/ListTemplatesContainer'
import ListTemplateDetailContainer from './ListTemplates/ListTemplateDetailContainer'
import ListTemplateDetailFormContainer from './ListTemplates/ListTemplateDetailFormContainer'
import SectionListsContainer from './SectionLists/SectionListsContainer';


class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Header />

          <Route exact path="/" render={() => (
            <ListsContainer/>
          )} />

          <Route exact path="/lists" render={() => (
            <ListsContainer/>
          )} />

          <Route exact path="/lists/:listId" render={({match}) => (
            <ListDetailContainer listId={match.params.listId} />
          )} />


          <Route exact path="/listEdit/:listId" render={({match}) => (
            <ListDetailFormContainer listId={match.params.listId} mode="Edit" />
          )} />

          <Route exact path="/listsNew" render={() => (
            <ListDetailFormContainer mode="Add" />
          )} />

          { /* this would impact new list form 
                this would look a lot like existing shopping lists summary
                except the store source would differ
          */ }
          <Route exact path="/settings/listTemplates" render={() => (
            <ListTemplatesContainer />
          )} />

          { /* this would impact new list form 
                this would look a lot like existing shopping lists summary
                except the store source would differ
          */ }
          <Route exact path="/settings/listTemplates/:templateListId" render={({match}) => (
            <ListTemplateDetailContainer templateListId={match.params.templateListId} />
          )} />

          { /* this would look a lot like existing shopping lists edit form
                except the store source would differ
          */ }
          <Route exact path="/settings/listTemplatesEdit/:templateListId" render={({match}) => (
            <ListTemplateDetailFormContainer mode="Edit" templateListId={match.params.templateListId} />
          )} />

          { /* this would look a lot like existing shopping lists add form
                except the store source for updates would differ
          */ }
          <Route exact path="/settings/listTemplatesNew" render={() => (
            <ListTemplateDetailFormContainer mode="Add" />
          )} />

          <Route exact path="/settings/store_sections" render={() => (
            <SectionListsContainer mode="Read" />
          )} />

          <Route exact path="/settings/storeSectionsEdit" render={() => (
            <div>shopping list form placeholder</div>
          )} />

          <Route exact path="/login" render={() => (
            <div>login route placeholder</div>
          )} />

          <Route exact path="/password" render={() => (
            <div>forgot pwd route placeholder</div>
          )} />

          <Route exact path="/register" render={() => (
            <div>registration route placeholder</div>
          )} />

          {/* <Footer /> */}

        </div>
      </Router>
    );
  }
}

export default App;
