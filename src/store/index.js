import { combineReducers } from 'redux';
import { lists } from '../Lists/ListsReducers';
import { listTemplates } from '../ListTemplates/ListTemplatesReducers';
import { sectionLists } from '../SectionLists/SectionListsReducers';

export default combineReducers({
  lists,
  listTemplates,
  sectionLists,
});
