import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import FaEdit from 'react-icons/lib/fa/edit'
import FaMinusSquareO from 'react-icons/lib/fa/minus-square-o'
import FaPlusSquareO from 'react-icons/lib/fa/plus-square-o'

import Lists from '../Lists/components/Lists'
import '../App.css';

//import { fetchCategories, fetchPosts, filterPosts, sortPosts } from '../posts/PostActions'
//import Post from '../posts/Post'
//import { prettySortVotes, prettySortTime, sortList } from '../utils'

class ListsTemplatesContainer extends Component {

  state = {
  }

  //categoryClick = this.categoryClick.bind(this)
  
  componentDidMount() {
    //this.props.dispatch()
  }

  objToArray(obj) {
    let resultAr = []
    Object.keys(obj).forEach(ok => {
      resultAr.push(obj[ok])
    })

    return resultAr
  }

  render() {
    let listTemplatesAr = []
    if (this.props.listTemplates) {
      listTemplatesAr = this.objToArray(this.props.listTemplates)
    }

    return (
      <div className="main">
        <Link to="/settings/listTemplatesNew"><FaPlusSquareO className="list-item-icon-lg" /> New Template List</Link>
        <Lists lists={listTemplatesAr} type="template" />
      </div>
    )
  }

}

function mapStateToProps({ listTemplates }) {
  return {
    listTemplates: listTemplates.listTemplates
  }
}

export default connect(mapStateToProps)(ListsTemplatesContainer);

//                <FaEdit className="list-item-icon" /><FaMinusSquareO className="list-item-icon"/>
/*
        <ul>
          {listTemplatesAr.map(list => {
            let listLink = `/lists/${list.listId}`
            console.log('list.listId: ' + list.listId)
            return (
              <li key={list.listId}>
                <Link to={listLink}><span className="list-name">{list.listName}</span></Link>


              </li>
            )
          })}
        </ul>
*/
