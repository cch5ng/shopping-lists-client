import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import FaMinusSquareO from 'react-icons/lib/fa/minus-square-o'

import Button from '../App/Shared/Button/Button'
import SelectList from '../App/Shared/SelectList/SelectList'
import '../App.css';

//import { fetchCategories, fetchPosts, filterPosts, sortPosts } from '../posts/PostActions'
//import Post from '../posts/Post'
//import { prettySortVotes, prettySortTime, sortList } from '../utils'

class ListTemplateDetailContainer extends Component {

  state = {
  }

  //categoryClick = this.categoryClick.bind(this)
  
  componentDidMount() {
    //this.props.dispatch()
  }

  onChangeHandlerSelectSort(ev) {
    console.log('ev: ' + ev)
  }

  editButtonClickHandler(ev) {
    console.log('clicked edit btn')
  }

  render() {
    const selectOptions = [
      {label: 'Sort By', value: 'none'},
      {label: 'Store section (alphabetically)', value: 'alpha'},
      {label: 'Store section (custom settings)', value: 'custom'}
    ]
    const { templateListId } = this.props
    const link = `/settings/listTemplatesEdit/${templateListId}`
    let listItemsAr = []
    let listKeys = Object.keys(this.props.curList.listItemInputs)
    listKeys.forEach(mkey => {
      listItemsAr.push(this.props.curList.listItemInputs[mkey])
    })

    return (
      <div className="main">
        <h3>Template: {this.props.curList.listName}</h3>
        <div>
          <Link to={link}><Button label="Edit" onClickHandler={this.editButtonClickHandler} /></Link>
          <Button label="Delete" />
          <SelectList defVal="none" options={selectOptions} onChange={this.onChangeHandlerSelectSort} />
        </div>
        <ul>
          {listItemsAr.map((listItem, idx) => {
            let listOrder = idx + 1

            return (
              <li>
                <span className="list-order">{listOrder}</span>
                <span className="list-item">{listItem.name}</span>
                <span className="list-section">{listItem.section}</span>
                <FaMinusSquareO />
              </li>
            )
          })}
        </ul>
        <div>
          <Link to={link}><Button label="Edit" onClickHandler={this.editButtonClickHandler} /></Link>
          <Button label="Delete" />
          <select value="none">
            <option value="none">Sort By</option>
            <option value="alpha">Store section (alphabetically)</option>
            <option value="custom">Store section (custom settings)</option>
          </select>
        </div>
      </div>
    )
  }
}

function mapStateToProps({ listTemplates }, ownProps) {
  let templateListId = ownProps.templateListId
  console.log('templateListId: ' + templateListId)
  let curList = listTemplates.listTemplates[templateListId]

  return { listTemplates, curList }
}

export default connect(mapStateToProps)(ListTemplateDetailContainer);

// function mapStateToProps({ lists }, ownProps) {
//   let listId = ownProps.listId
//   let curList = lists.lists[listId]

//   return {
//     curList
//   }
// }

