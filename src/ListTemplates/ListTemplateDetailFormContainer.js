import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import uuidv1 from 'uuid/v1'

import Button from '../App/Shared/Button/Button'
import SelectList from '../App/Shared/SelectList/SelectList'
import InputText from '../App/Shared/InputText/InputText'
import '../App.css';

// TODO temp test reducer directly
import { receiveTemplateListCreate, receiveTemplateListEdit } from './ListTemplatesActions'
//import { prettySortVotes, prettySortTime, sortList } from '../utils'

// TODO is this better to store as array vs object?
// reason for using object is that the key (unique id) is being used to track updates to the state for onChange ev but such an id could also be derived from array (just get idx)
// obj is easier to update (b/c of complex h.ierarchy) and easier to send to redux
// but for display view probably want to convert this to array
// is it better to use uuid? and also should I probably include an id field per item
const initListItemInputs = {
      shoppingListItem0: {name: '', section: 'none'},
      shoppingListItem1: {name: '', section: 'none'},
      shoppingListItem2: {name: '', section: 'none'},
      shoppingListItem3: {name: '', section: 'none'},
      shoppingListItem4: {name: '', section: 'none'},
      shoppingListItem5: {name: '', section: 'none'},
      shoppingListItem6: {name: '', section: 'none'},
      shoppingListItem7: {name: '', section: 'none'},
      shoppingListItem8: {name: '', section: 'none'},
      shoppingListItem9: {name: '', section: 'none'},
      shoppingListItem10: {name: '', section: 'none'},
      shoppingListItem11: {name: '', section: 'none'},
      shoppingListItem12: {name: '', section: 'none'},
      shoppingListItem13: {name: '', section: 'none'},
      shoppingListItem14: {name: '', section: 'none'},
    }

// const selectOptions = [
//   {label: 'Order', value: 'none'},
//   {label: '1', value: '1'},
//   {label: '2', value: '2'},
//   {label: '3', value: '1'},
//   {label: '4', value: '2'},
//   {label: '5', value: '1'},
//   {label: '6', value: '2'},
// ]

const sectionOptions = [
  {label: 'Section', value: 'none'},
  {label: 'none', value: 'none'},
  {label: 'drinks', value: 'drinks'},
  {label: 'dairy', value: 'dairy'},
  {label: 'frozen', value: 'frozen'},
  {label: 'prepared', value: 'prepared'},
  {label: 'deli', value: 'deli'},
  {label: 'produce', value: 'produce'},
]

class ListTemplateDetailFormContainer extends Component {

  state = {
    mode: this.props.mode ? this.props.mode : 'create',
    listItemInputs: this.props.mode === 'Edit' ? this.props.editList.listItemInputs : initListItemInputs,
    listName: this.props.mode === 'Edit' ? this.props.editList.listName : ''
  }

  //categoryClick = this.categoryClick.bind(this)
  getInputValue = this.getInputValue.bind(this)
  listItemInputChangeHandler = this.listItemInputChangeHandler.bind(this)
  listNameInputChangeHandler = this.listNameInputChangeHandler.bind(this)
  onChangeHandlerSelectSection = this.onChangeHandlerSelectSection.bind(this)
  formSubmitHandler = this.formSubmitHandler.bind(this)
  clearForm = this.clearForm.bind(this)

  componentDidMount() {
    //this.props.dispatch()
  }

  listNameInputChangeHandler(ev) {
    let listName = ev.target.value

    this.setState({ listName })
  }

  listItemInputChangeHandler(ev) {
    let name = ev.target.value
    let id = ev.target.id

    this.setState(prevState => {
      let newInput2 = {}
      newInput2.name = name

      let newInput = {}
      newInput[id] = {...prevState.listItemInputs[id], ...newInput2}
      let newState = {...prevState.listItemInputs, ...newInput}
      return {listItemInputs: newState}
    })
  }

  getInputValue(id) {
    return this.state.listItemInputs[id]
  }

  formSubmitHandler(ev) {
    let listItemInputs = this.state.listItemInputs
    let listName = this.state.listName
    let requestBody
    let listId

    if (this.state.mode === "Add") {
      // adding parentId
      listId = uuidv1()
      for (let itemKey in listItemInputs) {
        listItemInputs[itemKey].parentId = listId
      }
      requestBody = { listId, listName, listItemInputs}
      // TODO api call
      // TODO update store
      this.props.receiveTemplateListCreate(requestBody)
      //this.props.receiveListCreate(requestBody)
    } else if (this.state.mode === "Edit") {
      listId = this.props.templateListId
      requestBody = { listId, listName, listItemInputs}
      // TODO api call
      // TODO update store
      this.props.receiveTemplateListEdit(requestBody)
    }

    // TODO clear the form afer submit (update api/store); clear to empty in case
    // next time opened as a new list form...
    this.clearForm('empty')
  }

  reformatSelectId(id) {
    let tempAr = id.split('Select')
    return tempAr.join('')
  }

  onChangeHandlerSelectSection(ev) {
    let id = ev.target.id
    let reformattedId = this.reformatSelectId(id)
    let section = ev.target.value
    this.setState(prevState => {
      let newInput2 = {}
      newInput2.section = section

      let newInput = {}
      newInput[reformattedId] = {...prevState.listItemInputs[reformattedId], ...newInput2}
      let newState = {...prevState.listItemInputs, ...newInput}
      return {listItemInputs: newState}
    })
  }

  renderForm() {
    let htmlResult = []

    for (let i = 0; i < 15; i++) {
      let key = 'shoppingListItem' + i.toString()
      let selectKey = 'shoppingListItemSelect' + i.toString()
      htmlResult.push(
        <li key={key} >
          <InputText defVal={this.state.listItemInputs[key].name} placeholderVal="item name" idVal={key} onChangeHandler={this.listItemInputChangeHandler} />
          <SelectList defVal={this.state.listItemInputs[key].section} idVal={selectKey} options={sectionOptions} onChange={this.onChangeHandlerSelectSection} />
        </li>
      )
          // TODO this may be unnecessary tracked to order the list for speed of shopping?
          // <select value="none">
          //   <option value="none">none</option>
          //   <option value="1">1</option>
          //   <option value="2">2</option>
          //   <option value="3">3</option>
          //   <option value="4">4</option>
          //   <option value="5">5</option>
          // </select>
    }
    return htmlResult
  }

  clearForm(clearMode = null) {
    let formClearMode = clearMode === "empty" ? clearMode : this.state.mode

    switch(formClearMode) {
      case "Edit":
        this.setState({
          listItemInputs: this.props.editList.listItemInputs,
          listName: this.props.editList.listName
        })
        break

      case "Add":
      case "empty":
      default:
        this.setState({
          listItemInputs: initListItemInputs,
          listName: ''
        })
        break
    }
  }

// TODO think about how to expand the list size (hard coded max list length at 15)
// but mostly likely the list length needs to be flexible
// ability to add set of empty input fields (like 5 at a time)

  render() {
    if (this.props.lists && this.props.lists.lists) {
      console.log('lists keys: ' + Object.keys(this.props.lists.lists))
    }
    return (
      <div className="main">
        <h3>{this.state.mode} Template List</h3>
        <div>
          <Button classVal="listDetailFormSaveBtn" onClickHandler={this.formSubmitHandler} label="Save" />
          <Button label="Cancel" onClickHandler={this.clearForm} />
        </div>
        <br />
        <InputText defVal={this.state.listName} placeholderVal="list name" onChangeHandler={this.listNameInputChangeHandler} />
        <ul>
          {this.renderForm()}
        </ul>
        <div>
          <Button classVal="listDetailFormSaveBtn" onClickHandler={this.formSubmitHandler}label="Save" />
          <Button label="Cancel" onClickHandler={this.clearForm} />
        </div>
      </div>
    )
  }

}

function mapStateToProps({ listTemplates }, ownProps) {
  let templateListId = ownProps.templateListId
  console.log('templateListId: ' + templateListId)
  let editList = listTemplates.listTemplates[templateListId]

  return { listTemplates, editList }
}

function mapDispatchToProps(dispatch, ownProps) {
  return bindActionCreators( 
    { receiveTemplateListCreate,
      receiveTemplateListEdit
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ListTemplateDetailFormContainer);
