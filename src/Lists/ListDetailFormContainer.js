import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import uuidv1 from 'uuid/v1'

import Button from '../App/Shared/Button/Button'
import SelectList from '../App/Shared/SelectList/SelectList'
import InputText from '../App/Shared/InputText/InputText'
import Checkbox from '../App/Shared/Checkbox/Checkbox'
import '../App.css';

// TODO temp test reducer directly
import { receiveListCreate, receiveListEdit } from '../Lists/ListsActions'

const initListItemInputs = {
      shoppingListItem0: {name: '', section: 'none', done: false},
      shoppingListItem1: {name: '', section: 'none', done: false},
      shoppingListItem2: {name: '', section: 'none', done: false},
      shoppingListItem3: {name: '', section: 'none', done: false},
      shoppingListItem4: {name: '', section: 'none', done: false},
      shoppingListItem5: {name: '', section: 'none', done: false},
      shoppingListItem6: {name: '', section: 'none', done: false},
      shoppingListItem7: {name: '', section: 'none', done: false},
      shoppingListItem8: {name: '', section: 'none', done: false},
      shoppingListItem9: {name: '', section: 'none', done: false},
      shoppingListItem10: {name: '', section: 'none', done: false},
      shoppingListItem11: {name: '', section: 'none', done: false},
      shoppingListItem12: {name: '', section: 'none', done: false},
      shoppingListItem13: {name: '', section: 'none', done: false},
      shoppingListItem14: {name: '', section: 'none', done: false},
    }

class ListDetailFormContainer extends Component {

  constructor(props) {
    super(props);

    this.state = {
      mode: this.props.mode ? this.props.mode : 'create',
      listItemInputs: this.props.mode === 'Edit' ? this.props.editList.listItemInputs : initListItemInputs, //listItemsAr
      listName: this.props.mode === 'Edit' ? this.props.editList.listName : ''
    }

    this.getInputValue = this.getInputValue.bind(this)
    this.listItemInputChangeHandler = this.listItemInputChangeHandler.bind(this)
    this.listNameInputChangeHandler = this.listNameInputChangeHandler.bind(this)
    this.onChangeHandlerSelectSection = this.onChangeHandlerSelectSection.bind(this)
    this.onChangeHandlerDoneCheckbox = this.onChangeHandlerDoneCheckbox.bind(this)
    this.formSubmitHandler = this.formSubmitHandler.bind(this)
    this.clearForm = this.clearForm.bind(this)
  }

  listNameInputChangeHandler(ev) {
    let listName = ev.target.value;
    this.setState({ listName });
  }

  listItemInputChangeHandler(ev) {
    let name = ev.target.value;
    let id = ev.target.id;
    console.log('id', id);

    this.setState(prevState => {
      let newInput2 = {};
      newInput2.name = name;

      let newInput = {};
      newInput[id] = {...prevState.listItemInputs[id], ...newInput2};
      let newState = {...prevState.listItemInputs, ...newInput};
      return {listItemInputs: newState};
    })
  }

  getInputValue(id) {
    return this.state.listItemInputs[id];
  }

  formSubmitHandler(ev) {
    let listItemInputs = this.state.listItemInputs;
    let listName = this.state.listName;
    let requestBody;
    let listId;

    if (this.state.mode === "Add") {
      // adding parentId
      listId = uuidv1();
      for (let itemKey in listItemInputs) {
        listItemInputs[itemKey].parentId = listId;
      }
      requestBody = { listId, listName, listItemInputs};
      // TODO api call
      // TODO update store
      this.props.receiveListCreate(requestBody);
      this.clearForm('empty');
    } else if (this.state.mode === "Edit") {
      listId = this.props.listId;

      console.log('listItemInputs', listItemInputs);
      requestBody = { listId, listName, listItemInputs};
      this.props.receiveListEdit(requestBody);
    }

    // TODO clear the form afer submit (update api/store); clear to empty in case
    // next time opened as a new list form...
  }

  reformatSelectId(id) {
    let tempAr = id.split('Select')
    return tempAr.join('')
  }

  onChangeHandlerSelectSection(ev) {
    let id = ev.target.id
    let reformattedId = this.reformatSelectId(id)
    let section = ev.target.value
    this.setState(prevState => {
      let newInput2 = {}
      newInput2.section = section

      let newInput = {}
      newInput[reformattedId] = {...prevState.listItemInputs[reformattedId], ...newInput2}
      let newState = {...prevState.listItemInputs, ...newInput}
      return {listItemInputs: newState}
    })
  }

  onChangeHandlerDoneCheckbox(ev) {
    let checkboxId = ev.target.id

    this.setState(prevState => {
      let newCheckboxState = {};
      newCheckboxState.listItemInputs = prevState.listItemInputs;
      newCheckboxState['listItemInputs'][checkboxId]['done'] = !prevState['listItemInputs'][checkboxId]['done']

      return newCheckboxState;
    })
  }

  renderForm() {
    let htmlResult = [];
    let foodItemObjects;
    let sectionOrderAr = [];
    let listItemsAr = [];
    let { editList, sectionLists } = this.props;
    let sectionOptions = [{label: 'Section', value: 'none'}];

    // map cur section order to array idx (order 1 maps to idx 0)
    if (sectionLists) {
      let sections = sectionLists['id00001']['sections'];
      Object.keys(sections).forEach(id => {
        let order = sections[id]['order'];
        sectionOrderAr[order - 1] = sections[id]['name'];

        let sectionObj = {};
        sectionObj.label = sections[id]['name'];
        sectionObj.value = sections[id]['name'];
        sectionOptions.push(sectionObj);
      })
    }

    // order listItemsAr by food item's associated section
    if (editList) {

      let listKeys = Object.keys(this.state.listItemInputs)
      sectionOrderAr.forEach(section => {
        listKeys.forEach(mkey => {
          if (this.state.listItemInputs[mkey]['section'] === section) {
            listItemsAr.push(this.state.listItemInputs[mkey]);
          }
        })
      })

      listKeys.forEach(k => {
          if (this.state.listItemInputs[k]['section'] === 'none') {
            listItemsAr.push(this.state.listItemInputs[k]);
          }
      })


      listItemsAr.forEach(item => {
        const prefix = `shoppingListItem`;
        if (item.id) {
          let idNum = item.id.split('shoppingListItem')[1];
          let selectKey = `${prefix}Select${idNum}`;
          htmlResult.push(
            <li key={item.id} className="row">
              <Checkbox checkboxVal={this.state.listItemInputs[item.id].done} 
                onChangeHandler={this.onChangeHandlerDoneCheckbox} itemId={item.id} />
              <InputText defVal={this.state.listItemInputs[item.id].name} placeholderVal="item name" idVal={item.id} onChangeHandler={this.listItemInputChangeHandler} />
              <SelectList defVal={this.state.listItemInputs[item.id].section} idVal={selectKey} options={sectionOptions} onChange={this.onChangeHandlerSelectSection} />
            </li>
          );          
        }
      });
      return htmlResult;
    }
  }

  clearForm(clearMode = null) {
    let formClearMode = clearMode === "empty" ? clearMode : this.state.mode

    switch(formClearMode) {
      case "Edit":
        this.setState({
          listItemInputs: this.props.editList.listItemInputs,
          listName: this.props.editList.listName
        })
        break

      case "Add":
      case "empty":
      default:
        this.setState({
          listItemInputs: initListItemInputs,
          listName: ''
        })
        break
    }
  }

// TODO think about how to expand the list size (hard coded max list length at 15)
// but mostly likely the list length needs to be flexible
// ability to add set of empty input fields (like 5 at a time)

  render() {
    // if (this.props.lists && this.props.lists.lists) {
    // }

    return (
      <div className="main">
        <h3>{this.state.mode} List</h3>
        <div>
          <Button classVal="listDetailFormSaveBtn" onClickHandler={this.formSubmitHandler} label="Save" />
          <Button label="Cancel" onClickHandler={this.clearForm} />
        </div>
        <br />
        <InputText defVal={this.state.listName} placeholderVal="list name" onChangeHandler={this.listNameInputChangeHandler} />
        <ul className="list-no-style" >
          {this.renderForm()}
        </ul>
        <div>
          <Button classVal="listDetailFormSaveBtn" onClickHandler={this.formSubmitHandler}label="Save" />
          <Button label="Cancel" onClickHandler={this.clearForm} />
        </div>
      </div>
    )
  }

}

function mapStateToProps({ lists, sectionLists }, ownProps) {
  let listId = ownProps.listId;
  let editList = lists.lists[listId];
  //let sortedEditList = [];
  //let sectionSelectList = [];

  return {
    lists,
    editList,
    //listItemsAr,
    sectionLists: sectionLists.sectionLists,
  }
}

function mapDispatchToProps(dispatch, ownProps) {
  return bindActionCreators( 
    { receiveListCreate: receiveListCreate,
      receiveListEdit: receiveListEdit
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ListDetailFormContainer);
