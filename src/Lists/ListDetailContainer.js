import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import FaMinusSquareO from 'react-icons/lib/fa/minus-square-o'

import Button from '../App/Shared/Button/Button'
import SelectList from '../App/Shared/SelectList/SelectList'
import ListItem from './components/ListItem'
import '../App.css';

//import { fetchCategories, fetchPosts, filterPosts, sortPosts } from '../posts/PostActions'
//import Post from '../posts/Post'
//import { prettySortVotes, prettySortTime, sortList } from '../utils'

class ListDetailContainer extends Component {

  // set this
  //onChangeHandlerDoneCheckbox = this.onChangeHandlerDoneCheckbox.bind(this)

  //componentDidMount() {
    //this.props.dispatch()
  //}

  onChangeHandlerSelectSort(ev) {
    console.log('ev: ' + ev)
  }

  editButtonClickHandler(ev) {
    console.log('clicked edit btn')
  }

  render() {
    const selectOptions = [
      {label: 'Sort By', value: 'none'},
      {label: 'Store section (alphabetically)', value: 'alpha'},
      {label: 'Store section (custom settings)', value: 'custom'}
    ]
    const { listId, curList } = this.props
    const link = `/listEdit/${listId}`;
    let foodItemObjects;
    let listItemsAr = [];
    let sectionOrderAr = [];

    if (this.props.curList.listItemInputs) {
      foodItemObjects = this.props.curList.listItemInputs;
      console.log('foodItemObjects', foodItemObjects);
    }


    // map cur section order to array idx (order 1 maps to idx 0)
    if (this.props.sectionLists) {
      let sections = this.props.sectionLists['id00001']['sections'];
      console.log('sections', sections);
      Object.keys(sections).forEach(id => {
        let order = sections[id]['order'];
        sectionOrderAr[order - 1] = sections[id]['name'];
      })
      console.log('sectionOrderAr', sectionOrderAr);
    }

    // order listItemsAr by food item's associated section
    let listKeys = Object.keys(foodItemObjects)
    sectionOrderAr.forEach(section => {
      listKeys.forEach(mkey => {
        if (this.props.curList.listItemInputs[mkey]['section'] === section) {
          listItemsAr.push(this.props.curList.listItemInputs[mkey])
        }
      })
    })

    listKeys.forEach(k => {
        if (this.props.curList.listItemInputs[k]['section'] === 'none') {
          listItemsAr.push(this.props.curList.listItemInputs[k])
        }      
    })

    return (
      <div className="main">
        <h3>{curList.listName} List</h3>
        <div>
          <Link to={link}><Button label="Edit" onClickHandler={this.editButtonClickHandler} /></Link>
          <Button label="Delete" />
          <SelectList defVal="custom" options={selectOptions} onChange={this.onChangeHandlerSelectSort} />
        </div>
        <ul className="list-no-style" >
          {listItemsAr.map((listItem, idx) => {
            let listOrder = idx + 1
            let prop = `shoppingListItem${idx}`

            return (
              <ListItem key={prop} listOrder={listOrder} item={listItem} />
            )
          })}
        </ul>
        <div>
          <Link to={link}><Button label="Edit" onClickHandler={this.editButtonClickHandler} /></Link>
          <Button label="Delete" />
          <SelectList defVal="custom" options={selectOptions} onChange={this.onChangeHandlerSelectSort} />
        </div>
      </div>
    )
  }
}

function mapStateToProps({ lists, sectionLists }, ownProps) {
  let listId = ownProps.listId
  let curList = lists.lists[listId]

  return {
    curList,
    sectionLists: sectionLists.sectionLists,
  }
}

export default connect(mapStateToProps)(ListDetailContainer);
