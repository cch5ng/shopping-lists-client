import React, { Component } from 'react';
import FaMinusSquareO from 'react-icons/lib/fa/minus-square-o'
import './style.css';

const ListItem = ({ listOrder, item, itemId }) => {
  let key = `${listOrder}${item.name}`
  let name = item.name

  return (
    <li key={key} className="detail-list-item">
      <span>{name}</span>
      {item.section !== "none" && (<span className="list-section">({item.section})</span>)}
    </li>
  )
}

export default ListItem
