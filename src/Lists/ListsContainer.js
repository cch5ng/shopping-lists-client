import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import FaEdit from 'react-icons/lib/fa/edit'
import FaMinusSquareO from 'react-icons/lib/fa/minus-square-o'
import FaPlusSquareO from 'react-icons/lib/fa/plus-square-o'
import uuidv1 from 'uuid/v1'

import Lists from './components/Lists'
import SelectList from '../App/Shared/SelectList/SelectList'
import '../App.css';

import { receiveListCreate, receiveListDelete } from '../Lists/ListsActions'
//import { fetchCategories, fetchPosts, filterPosts, sortPosts } from '../posts/PostActions'
//import Post from '../posts/Post'
//import { prettySortVotes, prettySortTime, sortList } from '../utils'

class ListsContainer extends Component {

  state = {
  }

  onChangeHandlerSelectSort = this.onChangeHandlerSelectSort.bind(this)
  onClickHandlerDeleteList = this.onClickHandlerDeleteList.bind(this)

  componentWillReceiveProps(nextProps) {
    let lenNextPropsLists = Object.keys(nextProps.lists).length
    let lenCurPropsLists = Object.keys(this.props.lists).length
    let newId
    let newList

    // check if new list was added by selecting a template
    if (lenNextPropsLists > lenCurPropsLists) {
      newList = Object.keys(nextProps.lists).filter(newId => Object.keys(this.props.lists).indexOf(newId) === -1)
      newId = newList[0]
      // if new template list, then redirect to edit form for any customizations
      this.props.history.push(`/listEdit/${newId}`)
    }
  }

  onChangeHandlerSelectSort(ev) {
    let templateId = ev.target.value
    let listId = uuidv1()
    let listName = `Copy of ${this.props.listTemplates[templateId].listName}`
    let listItemInputs = this.props.listTemplates[templateId].listItemInputs
    let requestBody = { listId, listName, listItemInputs}

    this.props.receiveListCreate(requestBody)
  }

  onClickHandlerDeleteList(ev) {
    console.log('clicked delete list btn');
    console.log('ev.target.id: ' + ev.target.id)
    let listId = ev.target.id
    this.props.receiveListDelete(listId)
  }

  objToArray(obj) {
    let resultAr = []
    Object.keys(obj).forEach(ok => {
      resultAr.push(obj[ok])
    })

    return resultAr
  }

  render() {
    let listsAr = []
    if (this.props.lists) {
      listsAr = this.objToArray(this.props.lists)
    }
    let selectOptions = [{label: 'Add List by Template', value: 'none'}]
    let { listTemplates } = this.props

    Object.keys(listTemplates).forEach(template => {
      let optionObj = {}
      optionObj['label'] = listTemplates[template].listName
      optionObj['value'] = listTemplates[template].listId
      selectOptions.push(optionObj)
    })

    return (
      <div className="main">
        <SelectList defVal="none" options={selectOptions} onChange={this.onChangeHandlerSelectSort} />
        <Link to="/listsNew"><FaPlusSquareO className="list-item-icon-lg" /> New List</Link>
        <Lists lists={listsAr} type="shopping" clickHandlerDelete={this.onClickHandlerDeleteList} />
      </div>
    )
  }

}

function mapStateToProps({ lists, listTemplates }) {
  return {
    lists: lists.lists,
    listTemplates: listTemplates.listTemplates
  }
}

function mapDispatchToProps(dispatch, ownProps) {
  return bindActionCreators( 
    { receiveListCreate, receiveListDelete
    }, dispatch)
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListsContainer))
